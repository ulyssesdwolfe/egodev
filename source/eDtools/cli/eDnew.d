//Copyright (c) <2016>, <Ulysses D. Wolfe>
//All rights reserved.

/**
  * Create a new app to track in egoDev
  */
module eDnew;

import std.stdio;

int eDnew_main(string[] _args ){
    writeln("entered the eDnew_main function\n\t", _args);
    return 0;
}
