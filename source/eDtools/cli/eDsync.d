//Copyright (c) <2016>, <Ulysses D. Wolfe>
//All rights reserved.

/**
  * Interface wrapper to the VCS backend
  */
module eDsync;

import std.stdio;
import std.algorithm.searching : find;
import std.getopt;

import eDhelp;


/**
  * Global Variables
  */

auto eDvcs = "hg";

/**
  * Function Declarations
  */
int eDsync_main(string[] _args){
    
version(Win64){
        //TODO
        //auto eDinstall_path = executeShell("echo $HOME");
        //if (eDinstall_path.status != 0) writeln("Failed to retrieve user home directory");
}
version(linux ){ 

        bool help = false;
try{
            auto parseoptions = getopt(_args,"v|vcs", "Version Control System", &eDvcs);
        if ( parseoptions.helpWanted )
        {
            eDhelp_displayusage("sync", _args);
            defaultGetoptPrinter("\negodev sync <option>\n", parseoptions.options);
                return 0;
        }
        else if ( find(_args, "help") ){
            eDhelp_sync("sync", _args);
            defaultGetoptPrinter("\negodev sync <option>\n", parseoptions.options);
            return 0;

        }    
}
catch{
            eDhelp_sync("sync", _args);
            return 1;
}            
            /**VCS call*/
        
        writeln(eDvcs);
        

} // version ( linux ) end
   return 0; 
}

