//Copyright (c) <2016>, <Ulysses D. Wolfe>
//All rights reserved.

/**
  * Interface to apply a profile stored in eDconf.json to the current installation
  */

module eDprofile;

import std.stdio;
import std.json;

/** Commands
  * help -> Print the help info
  * new -> create a new profile
  * add a new entry to the profile
  * remove an entry from the profile
  */

private immutable string[] commands = ["help", "new", "add", "remove"];

int eDprofile_main(string[] _args ){
debug{
    writeln("**************************************************");
    writeln("******* Profile Input Arguements *****************");
    writeln(_args);
    writeln("**************************************************");
}
    try{
        assert(_args.length >= 1);

    }
    catch(Throwable){
    }
    
    File profile = File(_args[1], "w+");
    profile.writeln ("HERE");

    writeln("Here\n\t", _args);
    return 0;
}

