//Copyright (c) <2016>, <Ulysses D. Wolfe>
//All rights reserved.

/**
  * Install egodev from scratch using eDconf.json in root home directory
  */
module eDinstall;

import std.stdio, std.getopt, std.string;
import std.path,std.file;
import std.process;

import eDhelp;

///Global variables
auto eDinstall_path = "$HOME/.config/egodev";


/**
  * Install all files using <string[] arg> for default egodev files
  */
int eDinstall_install(string _dir, string _file){
    //_dir = chomp(_dir, "/");
    writeln(_dir,"**", _file);
    writeln(buildPath( _dir ,"/.config/egodev"));
    try{
        mkdir(_dir);
    }
    catch(Throwable){
        writeln("mkdir exception");
    }

    return 0;
}


/**
  * eDInstall Entry Point
  */
int eDinstall_main(string[] args){
    
    /// Validate input


    writeln(args);
    return 0;
}

