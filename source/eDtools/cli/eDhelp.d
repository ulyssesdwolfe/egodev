/**
 *Copyright (c) <2016><Ulysses D. Wolfe>
 **/

// All rights reserved.
/**
  *
  * Help $(B tyuy) Logic $0 for cli tool
  *
  */
module eDhelp;


import std.stdio;
import std.getopt;

/// Global Variables
string eDhelp_version = "0.0.1";

/++
 + Print the egodev logo to standard out
 + ---
 + import std.stdio;
 + auto writeln('here');
 + ---
 +/
void eDhelp_displayname(){

    /// Display ASCII art name
  writeln("\n-----------------------------------------");
    writeln("                      o                 /");
    writeln("                      O                '");
   writeln(r"                      o                \");
    writeln("                      o                 `");
    writeln(".oOo. .oOoO .oOo. .oOoO  .oOo. `o   O    |");
    writeln("OooO' o   O O   o o   O  OooO'  O   o   /");
    writeln("O     O   o o   O O   o  O      o  O   '");
   writeln(r"`OoO' `OoOo `OoO' `OoO'o `OoO'  `o'     \");
    writeln("          O                              `");
    writeln("       OoO'                              |");
    writeln("                                        /");
    writeln("egoDev Version : ", eDhelp_version,"                  |");
   writeln(r"----------------------------------------\");

}


void eDhelp_displayusage(string _command, string[] _args){

    //Display Usage
    writefln("%s","\nUsage: egodev [ -hv ] | [ sync ] | [ install ] | [ profile ] | [ new  ]  ");


   writefln("%s", "\nAvailable Commands :");

    writeln("  help     Display this help information    ");
    writeln("  version  Display the version information    ");
    writeln("  sync     synchronise a currently installed profile    ");
    writeln("  install  install a new egodev installation   ");
    writeln("  profile  what constitutes a profile //TODO    ");
    writeln("  new      make a new profile    ");
    writeln("  modify   modify a profile    ");
    writeln("\nUse : 'egodev <command> help' for more information");
}

void eDhelp_unknown(string _command, string[] _args){
    writeln("\negodev Error: Unknown command or option '", _args[1],"'\n");

}


void eDhelp_sync(string _command, string[] _args){
    writeln("egodev sync");
    writeln("\nSync the current installation with repo");

    /**
      * This feature will provide the hooks into the VCS to provide
      * simple push pull functionality
      */
}

void eDhelp_install(string[] args){
    string dir = ""; 
    string file = "";
    writeln("\negodev install [d|dir] [f|file]");
    writeln("\nNew installation in <dir> using configuration in <file>.");
    try{
        auto parsedargs = getopt(args, "d|dir", "Install Directory, defaults to '.config/egodev/", &dir, "f|file", "egoDev configuration file, defaults to 'egodev/eDconf.json'", &file);
        defaultGetoptPrinter("\nOptions", parsedargs.options);
    }
    catch(Throwable){
        writeln("\n------------------------------\n");
        writeln("\nError: unrecoginized option");
         writeln(r"        'egodev install -h' for help");
    }
}
void eDhelp_profile(){
    writeln("******************************");
    writeln("egodev profile");
    writeln("******************************");
    writeln("\nNew user profile ");

    //TODO
    /**
      * Provide an interface to modify the eDconf.json file
      *
      */
}
void eDhelp_new(){
    writeln("******************************");
    writeln("egodev new");
    writeln("******************************");
    writeln("\nCreate a new app profile");
    //TODO
    /**
      * Provide an interface to create new app configurations for 
      * tracking
      */
}


void eDhelp_modify(){
    writeln("******************************");
    writeln("egodev modify");
    writeln("******************************");
    writeln("\nModify an app profile");
    //TODO
    /** 
      * Provide an interface to modify the configuration of an app
      */
    


}

