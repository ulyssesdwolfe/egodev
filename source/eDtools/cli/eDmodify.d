//Copyright (c) <2016>, <Ulysses D. Wolfe>
//All rights reserved.

/**
  * Interface to modify an apps configuration json file
  */
module eDmodify;
import std.stdio;

int eDmodify_main(string[] _args ){
    writeln("entered the eDmodify_main function\n\t", _args);
    return 0;
}
