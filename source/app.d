/** 
 * Entry to egodev program 
 */


module app;

import std.system;
import std.file;
import std.stdio;
import std.algorithm.searching : find;
import std.getopt;


public import eDtools; 

string eDcli_config = "humpty";    // egoDev configuration file Path
string eDcli_av = "dumpty";   // egoDev apps-available Path
string eDcli_en = "sat";      // egoDev apps-enabled Path
bool eDcli_badinput=false; // bad input flag
immutable string[] eDcli_commands = ["help","version","sync","install","profile","new","modify","--help","--version","-h","-v"];


/// Version Variable
string egoDev_Version = "0.0.1";

/**
  * Entry point for the entire program
  *
  * Date: 02-07-2017
  * Returns: $(UL Return codes: 
    $(LI 0 on clean exit)
    $(LI $(RED 2 on eDerror: Unknown command or option)))$0
  */
int main(string[] _args)
{
    //auto path = getcwd();
//	writeln(path);
    /** for config directory **/


    /** Error Handling if input fails In Contract. **/
    try{
        assert( _args.length >= 1 );
        auto valid_command = find( eDcli_commands, _args[1] )[0]; //truncate the returned sorted array if > 0 to a single value.
        assert( valid_command.length );
    }
    catch(Throwable){
        eDcli_badinput = true;    
    }

    // detect bad input flag and exit
    if( eDcli_badinput ){
        
        try{ if( _args[1] ) {}}
        catch(Throwable) { 
            eDhelp_displayname;
            eDhelp_displayusage("egodev", _args);
            return 1;
        }

        writeln("\nError: Unknown command or option '",_args[1],"'\n");
        eDhelp_displayusage("egodev", _args);
        
        return 2;
    }



    /** Program Control Router: Once general input for args[1] has been determined to be safe, program control is routed to the correct handler.*/

    switch(_args[1]){

        /// default: Program Control will only be passed here by accident/error.
        default:
            throw new Exception("Unknown Command");
        /// Show Help
        case r"help":
            eDhelp_displayusage("egodev", _args);
            break;
        ///ditto
        case r"--help":
            eDhelp_displayusage("egodev", _args);
            break;
        ///ditto
        case r"-h":
            eDhelp_displayusage("egodev", _args);
            break;
        /// Show Version
        case r"version":
            eDhelp_displayname;
            break;
        ///ditto
        case r"--version":
            eDhelp_displayname;
            break;
        ///ditto
        case r"-v":
            eDhelp_displayname;
            break;
        /// Pass control to the eDsync command
        case r"sync":
            eDsync_main(_args[1 .. _args.length]);
            break;
        /// Pass control to the eDinstall command
        case r"install":
            eDinstall_main(_args[1 .. _args.length]);
            break;
        /// Pass control to the eDprofile command
        case r"profile":
            eDprofile_main(_args[1 .. _args.length]);
            break;  
        /// Pass control to the eDnew command
        case r"new":
            eDnew_main(_args[1 .. _args.length]);
            break;
        /// Pass control to the eDmodify command
        case r"modify":
            eDmodify_main(_args[1 .. _args.length]);
            break;
        }
    return 0;
}
