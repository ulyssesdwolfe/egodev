#EGODEV
######

This application is a configuration file manager written in [Dlang](http://dlang.org).

The original aim for the project is primarily to track and synchronize rc files across multiple environments. An example of which is the perennial favorite ~/.vimrc

Supported Environments
======================

Linux
-----
    *   Debian 8.7

Installation using dub
======================
Install Dub package manager [DUB](https://code.dlang.org/download)
add egodev in dub.json dependancies field
"dependancies":{
    "egodev":~>"0.0.1"
}

Standalone Installation
=======================
Download a release from the repo
[BB Repo](https://bitbucket.org/ulyssesdwolfe/egodev/downloads)
unzip contents and link to executable in bin folder

Developing egodev
=================

Clone the repo [egodev](https://bitbucket.org/ulyssesdwolfe/egodev)
Install hg flow for repo managements [hg flow](https://bitbucket.org/yujiewu/hgflow/downloads/)





### <TODO> List








